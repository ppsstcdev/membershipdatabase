﻿namespace PPSSTC.Models
{
    public enum Subspecialty
    {
        General_Pediatrics,
        Adolescent_Pediatrics,
        Allergology_and_Immunology,
        Ambulatory_Pediatrics,
        Cardiology,
        Critical_Care,
        Developmental_and_Behavioral_Pediatrics_or_Neurodevelopmental_Pediatrics,
        Endocrinology_and_Metabolism,
        Gastroentorology_Hepatology_and_Nutrition,
        Hematology,
        Hema_Oncology,
        Infectious_Disease,
        Newborn_Medicine,
        Nephrology,
        Neurology,
        Oncology,
        Pulmonology,
        Rheumatology
    }
}
