﻿namespace PPSSTC.Models
{
    public enum CpdUnitType
    {
        Units,
        NotApplicable,
        Pending
    }
}