﻿using Raven.Imports.Newtonsoft.Json;
using Raven.Imports.Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace PPSSTC.Models.Converters
{
    public class EventsConverter : JsonConverter
    {

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            object retVal = new List<string>();
            if (reader.TokenType == JsonToken.StartObject)
            {
                string instance = (string)serializer.Deserialize(reader, typeof(string));
                retVal = new List<string>() { instance };
            }
            else if (reader.TokenType == JsonToken.StartArray)
            {
                retVal = serializer.Deserialize(reader, objectType);
            }
            return retVal;

        }
        public override bool CanConvert(Type objectType)
        {
            return true;

        }

        public override bool CanWrite { get { return false; } }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

    }
}
