﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PPSSTC.Models
{
    public class TbPatient
    {
        // Demographics
        public string Id { get; set; }
        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Age is required")]
        public int Age { get; set; }
        [Required(ErrorMessage = "Birthday is required")]
        public DateTime Birthday { get; set; }
        public string Gender { get; set; }
        public Area Residence { get; set; }
        public string ResidenceText { get; set; }
        public string DoctorId { get; set; }
        public string DoctorName { get; set; }
        public string Comments { get; set; }

        // History
        public bool Exposure { get; set; }
        public string ExposureText { get; set; }
        public DateTime DiseaseOnset { get; set; }
        // Symptoms
        public bool Cough { get; set; }
        public bool Fever { get; set; }
        public bool LossOfAppetite { get; set; }
        public bool FailureToRespondToAntibiotics { get; set; }
        public bool FailureToRegainStateOfHealth { get; set; }
        public bool Fatigue { get; set; }
        public bool AbnominalSwelling { get; set; }
        public bool BoneOrJointLesions { get; set; }
        public bool CnsSymptoms { get; set; }

        //Diagnostics
        public SkinTest Skintest { get; set; }
        public Xray Xray { get; set; }
        public string XrayText { get; set; }
        public DirectSpectrumSmear DirectSpectrumSmear { get; set; }
        public string OtherDiagnostics { get; set; }

        // Treatment
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Treatment Treatment { get; set; }
        public string Treatmenttext { get; set; }
        public TreatmentOutcome TreatmentOutcome { get; set; }
        public string AdverseDrugReactions { get; set; }
        public Government Government { get; set; }

        public bool Deleted { get; set; }

        public string ToCsvString()
        {
            var separator = ",";
            //"First Name,Middle Name,Last Name,Age,Birthday,Gender,Residence,Residence if Other,Comments,Exposure,Exposure if yes,Disease Onset,Persistent cough/wheezing of 2 weeks or more,Unexplained fever (>38C) or 2 weeks or more,Loss of appetite or failure to gain weight,Failure to repsond to 2 weeks of appropriate antibiotic therapy for lower respiratory tract infection,Failure to regain previous state of health 2 weeks after viral infection or exanthem,Fatigure or reduced playfulness or lethargy,Abdominal swellings, hard painless mass and free fluid,Boint or Join Lesions,Cns Symptoms,Skintest,Xray,Xray if with comments,Direct Spectrum Smear Microscopy,Other Diagnostics,Start of Treatment,End of Treatment,Treatment,Treatment if others,Treatment Outcome,Adverse Drug Reactions,Reported to the government?";
            return FirstName + separator +
                MiddleName + separator + 
                LastName + Age + separator +
                Birthday.ToShortDateString() + separator + 
                Gender + separator + 
                Residence + separator +
                ResidenceText + separator + 
                Comments + separator + 
                Exposure +separator+ 
                ExposureText + separator + 
                DiseaseOnset.ToShortDateString() + separator + 
                Cough + separator +
                Fever + separator +
                LossOfAppetite + separator +
                FailureToRespondToAntibiotics + separator +
                FailureToRegainStateOfHealth + separator + 
                Fatigue + separator + 
                AbnominalSwelling + separator + 
                BoneOrJointLesions + separator + 
                CnsSymptoms + separator + 
                Skintest + separator + 
                Xray + separator + 
                XrayText + separator + 
                DirectSpectrumSmear + separator + 
                OtherDiagnostics + separator +
                StartDate.ToShortDateString() + separator +
                EndDate.ToShortDateString() + separator + 
                Treatment + separator + 
                Treatmenttext + separator + 
                TreatmentOutcome + separator + 
                AdverseDrugReactions + separator + 
                Government;
        }

    }
}
