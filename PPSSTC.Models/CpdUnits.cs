﻿using System;

namespace PPSSTC.Models
{
    public class CpdUnits
    {
        public string Name { get; set; }
        public CpdUnitType UnitType { get; set; }
        public float? Units { get; set; } = null;
        public DateTime Date { get; set; }
    }
}
