﻿using PPSSTC.Models.Converters;
using Raven.Imports.Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace PPSSTC.Models
{
    public class Member
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PRC { get; set; }
        public string PMA { get; set; }
        public Area Area { get; set; }
        public SubArea? SubArea { get; set; } = null;
        public string PhilHealth { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Subspecialty Subspecialty { get; set; }
        public Status Status { get; set; }
        public string Comments { get; set; }
        public bool Deceased { get; set; } = false;
        public string DateDeceased { get; set; }
        public string YearInducted { get; set; }

        public List<CpdUnits> Units { get; set; }
        public List<PaymentInfo> PaymentInfo { get; set; }
        public bool Deleted { get; set; }

        [JsonConverter(typeof(EventsConverter))]
        public List<string> EventAttendance { get; set; }

        public string AreaToReadableString(Area area)
        {
            switch (area)
            {
                case Area.Laguna_MLP: return "Laguna MLP";
                case Area.Batangas: return "Batangas";
                case Area.Cavite: return "Cavite";
                case Area.Island_Provinces: return "Island Provinces";
                case Area.Quezon: return "Quezon";
                default: return "others";
            }
        }
    }
}
