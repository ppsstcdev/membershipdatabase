﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PPSSTC.Models
{
    public enum TreatmentOutcome
    {
        Cured,
        CompletedTreatment,
        Defaulted,
        Died,
        Failed,
        TransferredOut,
        OnGoing
    }
}
