﻿namespace PPSSTC.Models
{
    public enum Area
    {
        Cavite,
        Batangas,
        Quezon,
        Laguna_MLP,
        Island_Provinces,
        Others
    }
}
