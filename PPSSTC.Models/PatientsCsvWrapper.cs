﻿namespace PPSSTC.Models
{
    public class PatientsCsvWrapper
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PRC { get; set; }
        public string PMA { get; set; }
        public Area Area { get; set; }
        public string PhilHealth { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public Subspecialty Subspecialty { get; set; }
        public Status Status { get; set; }
        public string DateDeceased { get; set; }
        public string DateOfBirth { get; set; }
    }
}
