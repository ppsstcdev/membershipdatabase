﻿namespace PPSSTC.Models
{
    public enum Status
    {
        Diplomate,
        Fellow,
        Emeritus_Fellow
    }
}
