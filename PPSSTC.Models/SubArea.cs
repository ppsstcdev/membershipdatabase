﻿namespace PPSSTC.Models
{
    public enum SubArea
    {
        Laguna_Proper,
        Muntinlupa,
        Las_Pinas,
        Paranaque,
        Oriental_Mindoro,
        Occidental_Mindoro,
        Marinduque,
        Romblon,
        Palawan,
    }
}
