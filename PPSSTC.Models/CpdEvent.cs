﻿using System;
using System.Collections.Generic;

namespace PPSSTC.Models
{
    public class CpdEvent
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Venue { get; set; }
        public CpdUnitType CpdUnitType { get; set; }
        public float? CpdCredits { get; set; } = null;
        public string Speakers { get; set; }
        public string Moderators { get; set; }
        public DateTime Date { get; set; }
        public bool IsDeleted { get; set; } = false;
        public List<string> MemberAttendees { get; set; }
        public List<EventAtendee> NonMemberAttendees{get;set;}
    }
}
