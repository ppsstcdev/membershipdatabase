﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PPSSTC.Models
{
    public enum DirectSpectrumSmear
    {
        Positive,
        Negative,
        NotDone
    }
}
