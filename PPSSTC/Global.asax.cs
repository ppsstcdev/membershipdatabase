﻿using FlexProviders.Aspnet;
using PPSSTC.Indeces;
using Raven.Client.Document;
using Raven.Client.Indexes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace PPSSTC
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        public static DocumentStore MemberStore;
        public static AspnetEnvironment Environment;

        protected void Application_BeginRequest()
        {
            var loadbalancerReceivedSslRequest = string.Equals(Request.Headers["X-Forwarded-Proto"], "https");
            var serverReceivedSslRequest = Request.IsSecureConnection;

            if (loadbalancerReceivedSslRequest || serverReceivedSslRequest) return;

            UriBuilder uri = new UriBuilder(Context.Request.Url);
            if (!uri.Host.Equals("localhost"))
            {
                uri.Port = 443;
                uri.Scheme = "https";
                Response.Redirect(uri.ToString());
            }
        }

        protected void Application_Start()
        {
            Environment = new AspnetEnvironment();
            MemberStore = new DocumentStore { ConnectionStringName = "MemberStore" };
            // MemberStore = new DocumentStore { Url = "http://localhost:8080/", DefaultDatabase = "MemberStore" }; 

            MemberStore.Initialize();

            IndexCreation.CreateIndexes(typeof(TbRegistryIndex).Assembly, MemberStore);
            IndexCreation.CreateIndexes(typeof(MembersIndex).Assembly, MemberStore);
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}