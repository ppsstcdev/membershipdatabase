﻿using System.Linq;
using PPSSTC.Models;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using System.Collections.Generic;

namespace PPSSTC.Indeces
{
    public class TbRegistryIndex : AbstractIndexCreationTask<TbPatient, TbRegistryIndex.TbRegistryResult>
    {
        public TbRegistryIndex()
        {
            Map = members => from member in members
                             select new
                                 {
                                     Name = new object[]
                                     {
                                         member.FirstName,
                                         member.LastName,
                                         member.MiddleName
                                     },
                                     member.LastName,
                                     member.Deleted
                                 };
            Index(x => x.Name, FieldIndexing.Analyzed);
            Index(x => x.LastName, FieldIndexing.Default);
            Index(x => x.Deleted, FieldIndexing.Default);

        }

        public class TbRegistryResult : TbPatient
        {
            public string Name;
        }
    }
}