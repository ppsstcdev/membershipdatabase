﻿using System.Linq;
using PPSSTC.Models;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using System.Collections.Generic;

namespace PPSSTC.Indeces
{
    public class MembersIndex : AbstractIndexCreationTask<Member,MembersIndex.MemberResult>
    {
        public MembersIndex()
        {
            Map = members => from member in members
                             select new
                                 {
                                     Name = new object[]
                                     {
                                         member.FirstName,
                                         member.LastName,
                                         member.MiddleName
                                     },
                                     member.PMA,
                                     member.PRC,
                                     member.Status,
                                     member.Subspecialty,
                                     member.Area,
                                     member.SubArea,
                                     member.LastName,
                                     member.PaymentInfo,
                                     paymentYears = member.PaymentInfo != null && member.PaymentInfo.Any() ? member.PaymentInfo.Select(x => x.Year).ToList() : null,
                                     member.Deleted,
                                     member.Deceased
                                 };

            Index(x => x.Name, FieldIndexing.Analyzed);
        }

        public class MemberResult : Member
        {
            public string Name;
            public List<int> paymentYears;
        }
    }
}