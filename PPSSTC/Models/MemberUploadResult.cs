﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPSSTC.Models
{
    public class MemberUploadResult
    {
        public int SuccessfulUploads { get; set; }
        public List<string> FailedUploads { get; set; }
        public string LastProcessed { get; set; }
        public string Error { get; set; }
    }
}