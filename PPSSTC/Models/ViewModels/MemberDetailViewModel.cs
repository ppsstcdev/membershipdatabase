﻿using System.Collections.Generic;

namespace PPSSTC.Models.ViewModels
{
    public class MemberDetailViewModel
    {
        public Member Member { get; set; }
        /// <summary>
        /// Grouped by Year - Month
        /// </summary>
        public Dictionary<string, List<CpdUnits>> CpdUnits { get; set; }

        public string GetSelectedCpdUnitType(CpdUnitType cpdUnitType)
        {
            if (cpdUnitType == CpdUnitType.Pending) return "CPD units pending";
            else if (cpdUnitType == CpdUnitType.NotApplicable) return "CPD units not applicable";
            else return "";
        }
    }
}