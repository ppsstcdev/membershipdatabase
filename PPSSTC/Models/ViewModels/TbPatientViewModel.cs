﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PPSSTC.Models.ViewModels
{
    public class TbPatientViewModel
    {
        public TbPatient Patient { get; set; }
  
        public List<SelectListItem> GetArea()
        {
            var options = new List<SelectListItem>();

            options.Add(new SelectListItem
            {
                Text = "Alabang / Laguna",
                Value = Area.Laguna_MLP.ToString(),
                Selected = true
            });

            options.Add(new SelectListItem
            {
                Text = "Batangas",
                Value = Area.Batangas.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Cavite",
                Value = Area.Cavite.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Island Provinces",
                Value = Area.Island_Provinces.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Quezon",
                Value = Area.Quezon.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Others",
                Value = Area.Others.ToString(),
            });

            return options;
        }


        public List<SelectListItem> GetGovernment()
        {
            var options = new List<SelectListItem>();

            options.Add(new SelectListItem
            {
                Text = "TB Dots",
                Value = Government.TBDots.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Health Center",
                Value = Government.HealthCenter.ToString(),
            });


            options.Add(new SelectListItem
            {
                Text = "No",
                Value = Government.No.ToString(),
                Selected = true
            });

            return options;
        }

        public List<SelectListItem> GetGender()
        {
            var options = new List<SelectListItem>();

            options.Add(new SelectListItem
            {
                Text = "Male",
                Value = "Male",
                Selected = true
            });

            options.Add(new SelectListItem
            {
                Text = "Female",
                Value = "Female",
            });

            return options;
        }

        public List<SelectListItem> GetExposure()
        {
            var options = new List<SelectListItem>();

            options.Add(new SelectListItem
            {
                Text = "Yes",
                Value = true.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "No",
                Value = false.ToString(),
                Selected = true
            });

            return options;
        }


        public List<SelectListItem> GetSkinTests()
        {
            var options = new List<SelectListItem>();

            options.Add(new SelectListItem
            {
                Text = "0 - 10mm",
                Value = SkinTest.ZeroToTen.ToString(),
            });
            options.Add(new SelectListItem
            {
                Text = "More than 10mm",
                Value = SkinTest.MoreThanTen.ToString(),
            });
            options.Add(new SelectListItem
            {
                Text = "Not done",
                Value = SkinTest.NotDone.ToString(),
                Selected = true
            });

            return options;
        }

        public List<SelectListItem> GetChestXray()
        {
            var options = new List<SelectListItem>();

            options.Add(new SelectListItem
            {
                Text = "Normal",
                Value = Xray.Normal.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Primary TB",
                Value = Xray.PrimaryTB.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Hilar lymphadenopathy",
                Value = Xray.Hilar.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Not done",
                Value = Xray.Notdone.ToString(),
                Selected =true
            });

            options.Add(new SelectListItem
            {
                Text = "Others",
                Value = Xray.Others.ToString(),
            });

            return options;
        }

        public List<SelectListItem> GetDirectSputumSmearMicroscopy()
        {
            var options = new List<SelectListItem>();

            options.Add(new SelectListItem
            {
                Text = "Positive",
                Value = DirectSpectrumSmear.Positive.ToString()
            });
            options.Add(new SelectListItem
            {
                Text = "Negative",
                Value = DirectSpectrumSmear.Negative.ToString(),
            });
            options.Add(new SelectListItem
            {
                Text = "Not done",
                Value = DirectSpectrumSmear.NotDone.ToString(),
                Selected = true
            });

            return options;
        }

        public List<SelectListItem> GetTreatment()
        {
            var options = new List<SelectListItem>();

            options.Add(new SelectListItem
            {
                Text = "2HRZ/4HR",
                Value = Treatment.HRZ.ToString(),
                Selected = true
            });
            options.Add(new SelectListItem
            {
                Text = "2HRZE or S/4HR",
                Value = Treatment.HRZE.ToString(),
            });
            options.Add(new SelectListItem
            {
                Text = "Others",
                Value = Treatment.Others.ToString(),
            });

            return options;
        }

        public List<SelectListItem> GetTreatmentOutcomes()
        {
            var options = new List<SelectListItem>();
            options.Add(new SelectListItem
            {
                Text = "On Going Treatment",
                Value = TreatmentOutcome.OnGoing.ToString(),
                Selected = true
            });
            options.Add(new SelectListItem
            {
                Text = "Cured",
                Value = TreatmentOutcome.Cured.ToString(),
            });
            options.Add(new SelectListItem
            {
                Text = "Completed Treatment",
                Value = TreatmentOutcome.CompletedTreatment.ToString(),
            });
            options.Add(new SelectListItem
            {
                Text = "Defaulted",
                Value = TreatmentOutcome.Defaulted.ToString(),
            });
            options.Add(new SelectListItem
            {
                Text = "Died",
                Value = TreatmentOutcome.Died.ToString(),
            });
            options.Add(new SelectListItem
            {
                Text = "Failed",
                Value = TreatmentOutcome.Failed.ToString(),
            });
            options.Add(new SelectListItem
            {
                Text = "Transferred out",
                Value = TreatmentOutcome.TransferredOut.ToString(),
            });

            return options;
        }
    }
}