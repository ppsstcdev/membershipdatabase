﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PPSSTC.Models.ViewModels
{
    public class EditMemberModel
    {
        public Member Member { get; set; }
        public List<SelectListItem> GetStatuses()
        {
            var statuses = from Status d in Enum.GetValues(typeof(Status))
                           select new SelectListItem { Value = d.ToString(), Text = d.ToString().Replace("_", " "), Selected = d.Equals(Member.Status) };
            return statuses.ToList();
        }

        public List<SelectListItem> GetSubspecialties()
        {
            var subspec = from Subspecialty d in Enum.GetValues(typeof(Subspecialty))
                          select new SelectListItem { Value = d.ToString(), Text = d.ToString().Replace("_", " ") };
            return subspec.ToList();
        }

        public string AreaToReadableString(Area area)
        {
            switch (area)
            {
                case Area.Laguna_MLP: return "Laguna MLP";
                case Area.Batangas: return "Batangas";
                case Area.Cavite: return "Cavite";
                case Area.Island_Provinces: return "Island Provinces";
                case Area.Quezon: return "Quezon";
                default: return "others";
            }
        }

        public List<SelectListItem> GetAreas()
        {
            var options = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "Laguna MLP",
                    Value = Area.Laguna_MLP.ToString(),
                },

                new SelectListItem
                {
                    Text = "Batangas",
                    Value = Area.Batangas.ToString(),
                },

                new SelectListItem
                {
                    Text = "Cavite",
                    Value = Area.Cavite.ToString(),
                },

                new SelectListItem
                {
                    Text = "Island Provinces",
                    Value = Area.Island_Provinces.ToString(),
                },

                new SelectListItem
                {
                    Text = "Quezon",
                    Value = Area.Quezon.ToString(),
                }
            };

            return options;
        }

        public List<SelectListItem> GetSubAreas(Area area)
        {
            var options = new List<SelectListItem>();

            if (area == Area.Laguna_MLP)
            {
                options.Add(new SelectListItem
                {
                    Text = "Laguna Proper",
                    Value = SubArea.Laguna_Proper.ToString(),
                });

                options.Add(new SelectListItem
                {
                    Text = "Muntinlupa",
                    Value = SubArea.Muntinlupa.ToString(),
                });

                options.Add(new SelectListItem
                {
                    Text = "Las Pinas",
                    Value = SubArea.Las_Pinas.ToString(),
                });

                options.Add(new SelectListItem
                {
                    Text = "Paranaque",
                    Value = SubArea.Paranaque.ToString(),
                });
            }
            else if (area == Area.Island_Provinces)
            {
                options.Add(new SelectListItem
                {
                    Text = "Oriental Mindoro",
                    Value = SubArea.Oriental_Mindoro.ToString(),
                });

                options.Add(new SelectListItem
                {
                    Text = "Occidental Mindoro",
                    Value = SubArea.Occidental_Mindoro.ToString(),
                });

                options.Add(new SelectListItem
                {
                    Text = "Marinduque",
                    Value = SubArea.Marinduque.ToString(),
                });

                options.Add(new SelectListItem
                {
                    Text = "Romblon",
                    Value = SubArea.Romblon.ToString(),
                });

                options.Add(new SelectListItem
                {
                    Text = "Palawan",
                    Value = SubArea.Palawan.ToString(),
                });
            }
            else 
            {
                options.Add(new SelectListItem
                {
                    Text = "No subarea/s available",
                    Value = null
                });
            }

            return options;
        }
    }
}
