﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace PPSSTC.Models.ViewModels
{
    public class MemberListViewModel
    {
        public string Name { get; set; }
        public string PRC { get; set; }
        public string PMA { get; set; }
        public Status? Status { get; set; }
        public Area? Area { get; set; }
        public SubArea? SubArea { get; set; }
        public Subspecialty? Subspecialty { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Must be a number")]
        public int? Year { get; set; }

        public List<Member> Members { get; set; }

        public string AreaToReadableString(Area area) {
            switch (area) {
                case Models.Area.Laguna_MLP: return "Laguna MLP";
                case Models.Area.Batangas: return "Batangas";
                case Models.Area.Cavite: return "Cavite";
                case Models.Area.Island_Provinces: return "Island Provinces";
                case Models.Area.Quezon: return "Quezon";
                default: return "others";
            }
        }

        public List<SelectListItem> GetStatuses()
        {
            var statuses = from Status d in Enum.GetValues(typeof(Status))
                           select new SelectListItem { Value = d.ToString(), Text = d.ToString().Replace("_", " ") };
            return statuses.ToList();
        }

        public List<SelectListItem> GetSubspecialties()
        {
            var subspec = from Subspecialty d in Enum.GetValues(typeof(Subspecialty))
                          select new SelectListItem { Value = d.ToString(), Text = d.ToString().Replace("_"," ") };
            return subspec.ToList();
        }

        public List<SelectListItem> GetAreas()
        {
            var options = new List<SelectListItem>();

            options.Add(new SelectListItem
            {
                Text = "Laguna MLP",
                Value = Models.Area.Laguna_MLP.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Batangas",
                Value = Models.Area.Batangas.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Cavite",
                Value = Models.Area.Cavite.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Island Provinces",
                Value = Models.Area.Island_Provinces.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Quezon",
                Value = Models.Area.Quezon.ToString(),
            });

            return options;
        }

        public List<SelectListItem> GetSubAreas()
        {
            var options = new List<SelectListItem>();

            options.Add(new SelectListItem
            {
                Text = "Laguna Proper",
                Value = Models.SubArea.Laguna_Proper.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Muntinlupa",
                Value = Models.SubArea.Muntinlupa.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Las Pinas",
                Value = Models.SubArea.Las_Pinas.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Paranaque",
                Value = Models.SubArea.Paranaque.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Oriental Mindoro",
                Value = Models.SubArea.Oriental_Mindoro.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Occidental Mindoro",
                Value = Models.SubArea.Occidental_Mindoro.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Marinduque",
                Value = Models.SubArea.Marinduque.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Romblon",
                Value = Models.SubArea.Romblon.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "Palawan",
                Value = Models.SubArea.Palawan.ToString(),
            });

            options.Add(new SelectListItem
            {
                Text = "No subarea/s available",
                Value = null
            });

            return options;
        }

        public int Page { get; set; }
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
    }
}