﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPSSTC.Models.ViewModels
{
    public class TbListViewModel
    {
        public List<TbPatient> Patients { get; set; }
        public string Name { get; set; }
        public int Page { get; set; }
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
    }
}