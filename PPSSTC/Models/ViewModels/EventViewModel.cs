﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PPSSTC.Models.ViewModels
{
    public class EventViewModel
    {
        public CpdEvent Event { get; set; }

        public List<SelectListItem> GetCpdUnitTypes()
        {
            var options = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Selected = true,
                    Text = "CPD units applicable",
                    Value = CpdUnitType.Units.ToString()
                },

                new SelectListItem
                {
                    Text = "CPD units pending",
                    Value = CpdUnitType.Pending.ToString()
                },

                new SelectListItem
                {
                    Text = "CPD units not applicable",
                    Value = CpdUnitType.NotApplicable.ToString()
                }
            };

            return options;
        }

        public string GetSelectedCpdUnitType(string value)
        {
            if (value == CpdUnitType.Units.ToString()) return "CPD units applicable";
            else if (value == CpdUnitType.Pending.ToString()) return "CPD units pending";
            else return "CPD units not applicable";
        }
    }
}