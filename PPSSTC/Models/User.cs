﻿using FlexProviders.Membership;
using Raven.Imports.Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace PPSSTC.Models
{
    public class User : IFlexMembershipUser, IPrincipal
    {
        private IIdentity _identity;

        public User()
        {
            OAuthAccounts = new Collection<FlexOAuthAccount>();
            Roles = new string[0];
        }

        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string PasswordResetToken { get; set; }
        public DateTime PasswordResetTokenExpiration { get; set; }
        public virtual ICollection<FlexOAuthAccount> OAuthAccounts { get; set; }

        public bool IsActivated { get; set; }

        public bool IsBanned { get; set; }
        public string Email { get; set; }

        [JsonIgnore]
        public IIdentity Identity
        {
            get { return _identity ?? (_identity = new GenericIdentity(Username)); }
            private set { _identity = value; }
        }

        [JsonIgnore]
        public IEnumerable<string> Roles { get; set; }

        public string ActivationKey { get; set; }

        public bool IsInRole(string role)
        {
            return Roles.Contains(role);
        }
    }
}