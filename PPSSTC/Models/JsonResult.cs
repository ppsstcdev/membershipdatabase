﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPSSTC.Models
{
    public class GenericJsonResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}