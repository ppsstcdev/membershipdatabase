﻿using PPSSTC.Indeces;
using PPSSTC.Models;
using PPSSTC.Models.ViewModels;
using Raven.Client;
using Raven.Client.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PPSSTC.Controllers
{
    [Authorize]
    public class TbRegistryController : RavenController
    {
        private int _pageSize = 15;

        public ActionResult Index(TbListViewModel model)
        {
            model.Page = 1;

            var newViewModel = BuildViewmodel(model);

            return View(newViewModel);
        }

        public ActionResult Delete(string id)
        {
            var user = MemberSession.Load<TbPatient>(id);
            if (id != null && user != null)
            {
                user.Deleted = true;
                MemberSession.Store(user);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(string id)
        {
            var ravenId = "TbPatients/" + id;
            var patient = MemberSession.Load<TbPatient>(ravenId);
            var viewModel = new TbPatientViewModel { 
            Patient = patient
            };

            return View(viewModel);
        }

        public ActionResult Details(string id)
        {
            var ravenId = "TbPatients/" + id;
            var patient = MemberSession.Load<TbPatient>(ravenId);
            var viewModel = new TbPatientViewModel
            {
                Patient = patient
            };

            return View(viewModel);
        }

        public Helpers.CsvActionResult<TbPatient> ExportData()
        {
            var page = 0;
            var pageSize = 100;
            var patients = new List<TbPatient>();
            var keepGoing = true;
            do
            {
                var currentPatients = MemberSession.Query<TbPatient>().Skip(page * pageSize).Take(pageSize);
                if (currentPatients.Any())
                {
                    patients.AddRange(currentPatients);
                    page++;
                }
                else {
                    keepGoing = false;
                }
            } while (keepGoing);

            var headers = "First Name,Middle Name,Last Name,Age,Birthday,Gender,Residence,Residence if Other,Comments,Exposure,Exposure if yes,Disease Onset,Persistent cough/wheezing of 2 weeks or more,Unexplained fever (>38C) or 2 weeks or more,Loss of appetite or failure to gain weight,Failure to repsond to 2 weeks of appropriate antibiotic therapy for lower respiratory tract infection,Failure to regain previous state of health 2 weeks after viral infection or exanthem,Fatigure or reduced playfulness or lethargy,Abdominal swellings, hard painless mass and free fluid,Boint or Join Lesions,Cns Symptoms,Skintest,Xray,Xray if with comments,Direct Spectrum Smear Microscopy,Other Diagnostics,Start of Treatment,End of Treatment,Treatment,Treatment if others,Treatment Outcome,Adverse Drug Reactions,Reported to the government?";
            var stringBuilder = new StringBuilder();

            var result = new Helpers.CsvActionResult<TbPatient>(patients, "TBRegistryExport.csv");
            return result;
        }

        [HttpPost]
        public ActionResult Edit(TbPatientViewModel model)
        {
            MemberSession.Store(model.Patient);
            return RedirectToAction("Index");
        }

        public ActionResult Add()
        {
            var viewModel = new TbPatientViewModel {Patient = new TbPatient() };
            var role = HttpContext.Request.Cookies.Get("role").Value;
            if (role.Equals("member"))
            {
                var memberId = HttpContext.Request.Cookies.Get("memberId").Value;
                var member = MemberSession.Load<Member>(memberId);
                viewModel.Patient.DoctorId = member.Id;
                viewModel.Patient.DoctorName = member.FirstName + " " + member.LastName;
            }
            else {
                var username = "Administrator";
                if (HttpContext.Request.Cookies.Get("username") != null) {
                    username = HttpContext.Request.Cookies.Get("username").Value;
                }
                viewModel.Patient.DoctorName = username;
            }

            viewModel.Patient.StartDate = DateTime.Now;
            viewModel.Patient.Birthday = DateTime.Now;
            viewModel.Patient.Xray = Xray.Notdone;
            viewModel.Patient.Skintest = SkinTest.NotDone;
            viewModel.Patient.TreatmentOutcome = TreatmentOutcome.OnGoing;
            viewModel.Patient.DirectSpectrumSmear = DirectSpectrumSmear.NotDone;
            viewModel.Patient.Government = Government.No;

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Add(TbPatientViewModel model)
        {
            MemberSession.Store(model.Patient);
            return RedirectToAction("Index");
        }

        public ActionResult Search(TbListViewModel model)
        {
            var newViewModel = BuildViewmodel(model);

            return View("Index", newViewModel);
        }

        private TbListViewModel BuildViewmodel(TbListViewModel model)
        {
            model.PageSize = _pageSize;
            var members = MemberSession.Query<TbRegistryIndex.TbRegistryResult, TbRegistryIndex>();

            members = members.Where(x => !x.Deleted);

            if (!string.IsNullOrEmpty(model.Name))
            {
                members = members.Search(x => x.Name, model.Name, escapeQueryOptions: EscapeQueryOptions.RawQuery);
            }

            RavenQueryStatistics stats;

            members = members.OrderBy(x => x.LastName).Statistics(out stats);

            var pagedMembers = members.As<TbPatient>().Skip((model.Page - 1) * model.PageSize).Take(model.PageSize).ToList();

            model.Patients = pagedMembers;

            model.TotalCount = stats.TotalResults;

            return model;
        }
    }
}
