﻿using FlexProviders.Aspnet;
using FlexProviders.Membership;
using FlexProviders.Roles;
using PPSSTC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PPSSTC.Controllers
{
    public class HomeController : RavenController
    {
        private IFlexMembershipProvider _flexMembershipProvider;
        private IFlexRoleProvider _roleProvider;

        //
        // GET: /Home/

        public ActionResult Index()
        {
            InitializeMemflex();
            return View(new LoginModel());
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            InitializeMemflex();

            // Normal login sequence for admin
            if (_flexMembershipProvider.HasLocalAccount(model.username))
            {
                if (_flexMembershipProvider.Login(model.username, model.password,true)) {
                    HttpContext.Response.Cookies.Add(new HttpCookie("username", model.username));
                    HttpContext.Response.Cookies.Add(new HttpCookie("role", "administrator"));
                    return RedirectToAction("Index", "Members");
                }
                TempData["error"] = "Invalid login credentials";
                return View("Index");
            }

            // Magic login for members as they don't have a member entity by design
            var member = MemberSession.Query<Member>().FirstOrDefault(x => x.LastName.Equals(model.username.Trim()) && x.PRC.Equals(model.password) && !x.Deleted);
            if (member != null)
            {
                HttpContext.Response.Cookies.Add(new HttpCookie("memberId", member.Id));
                HttpContext.Response.Cookies.Add(new HttpCookie("role", "member"));

                if (_flexMembershipProvider.Login("dummy", "p@ssw0rd32",true))
                {
                    // return RedirectToAction("Index", "TbRegistry");
                    // TODO: Place this back at milestone 2
                    return RedirectToAction("Details", "Members", new { id = member.Id.Split('/')[1] });
                }

            }

            TempData["error"] = "Invalid login credentials";
            return View("Index");
        }

        public ActionResult LogOut()
        {
            InitializeMemflex();
            _flexMembershipProvider.Logout();
            HttpContext.Request.Cookies.Remove("username");
            HttpContext.Request.Cookies.Remove("role");
            HttpContext.Request.Cookies.Remove("memberId");

            return RedirectToAction("Index");
        }

        public void InitializeMemflex()
        {
            // This can't be called inside the constructor because the sessions aren't alive there yet
            var userStore = new FlexProviders.Raven.FlexMembershipUserStore<Models.User, Models.Role>(MemberSession);
            _flexMembershipProvider = new FlexMembershipProvider(userStore, MvcApplication.Environment);
            _roleProvider = new FlexRoleProvider(userStore);
        }

    }
}
