﻿using FlexProviders.Membership;
using FlexProviders.Roles;
using PPSSTC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PPSSTC.Controllers
{
    public class AdministratorController : RavenController
    {
        //
        // GET: /Administrator/
        private IFlexMembershipProvider _flexMembershipProvider;
        private IFlexRoleProvider _roleProvider;

        public ActionResult Create(string username,string password)
        {
            InitializeMemflex();
            var result = new GenericJsonResult();
            try
            {
                if (_flexMembershipProvider.HasLocalAccount(username))
                {
                    result.Success = false;
                    result.Message = "An administrator with that username has already been added";
                    return Json(result);
                }

                _flexMembershipProvider.CreateAccount(new User
                {
                    Username = username,
                    Password = password,
                    IsActivated = true,
                });

                result.Success = true;
                result.Message = username + " has been added as an administrator";
            }
            catch
            {
                result.Success = false;
                result.Message = "Unable to create admin";
            }
            return Json(result);
        }

        public void InitializeMemflex()
        {
            // This can't be called inside the constructor because the sessions aren't alive there yet
            var userStore = new FlexProviders.Raven.FlexMembershipUserStore<Models.User, Models.Role>(MemberSession);
            _flexMembershipProvider = new FlexMembershipProvider(userStore, MvcApplication.Environment);
            _roleProvider = new FlexRoleProvider(userStore);
        }
    }
}
