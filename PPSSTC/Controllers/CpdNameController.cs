﻿using PPSSTC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace PPSSTC.Controllers
{
    public class CpdNameController : RavenController
    {
        //
        // GET: /CpdName/

        public ActionResult Index()
        {
            var cpdname = MemberSession.Query<CpdName>().FirstOrDefault();
            if (cpdname == null)
            {
                cpdname = new CpdName { Names = new List<string>() };
                MemberSession.Store(cpdname);
            }
            return Json(cpdname, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(string name)
        {
            try
            {
                var result = new GenericJsonResult();
                var cpdname = MemberSession.Query<CpdName>().FirstOrDefault();

                cpdname.Names.Add(name);
                MemberSession.Store(cpdname);

                result.Success = true;
                result.Message = "CPD Name added";

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new GenericJsonResult
                {
                    Message = ex.Message,
                    Success = false
                });
            }
        }

    }
}
