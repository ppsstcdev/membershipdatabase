﻿using ExcelDataReader;
using PPSSTC.Models;
using PPSSTC.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PPSSTC.Controllers
{
    public class EventsController : RavenController
    {
        //
        // GET: /Events/

        public ActionResult Index()
        {
            if (Request.Cookies.Get("role")
                .Value == "administrator")
            {
                var events = MemberSession.Query<CpdEvent>().Take(int.MaxValue).ToList();
                events = events.FindAll(x => !x.IsDeleted);
                events.Sort((x, y) => y.Date.CompareTo(x.Date));
                return View(events);
            }

            return RedirectToAction("Index", "Members");
        }

        public ActionResult Add()
        {
            if (Request.Cookies.Get("role")
                .Value == "administrator")
            {
                var viewModel = new EventViewModel { Event = new CpdEvent() };
                return View(viewModel);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Start(string id)
        {
            if (IsAdmin())
            {
                var viewModel = MemberSession.Load<CpdEvent>("CpdEvents/" + id);

                return View(viewModel);
            }

            return RedirectToAction("Index");
        }

        private bool IsAdmin()
        {
            return Request.Cookies.Get("role")
                                      .Value == "administrator";
        }


        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase File, string id)
        {
            var results = new MemberUploadResult
            {
                FailedUploads = new List<string>()
            };

            if (IsAdmin())
            {
                if (!FileIsValidForUpload(File))
                {
                    return View("UploadResults", new MemberUploadResult
                    {
                        Error = "File is either empty or not a .csv/.xls/.xlsx file. Cannot Proceed with the upload"
                    });
                }

                var cpdEvent = MemberSession.Load<CpdEvent>("CpdEvents/" + id);

                if (File.FileName.Contains(".xlsx") || File.FileName.Contains(".xls"))
                {
                    results = ProcessXlsx(File, cpdEvent);
                }
                else 
                {
                    StreamReader csvReader = new StreamReader(File.InputStream);

                    results = ProcessCsv(csvReader, cpdEvent);

                    csvReader.Close();
                }

                return View("UploadResults", results);
            }

            return RedirectToAction("Index");
        }

        private MemberUploadResult ProcessXlsx(HttpPostedFileBase file, CpdEvent cpdEvent)
        {
            var results = new MemberUploadResult
            {
                FailedUploads = new List<string>()
            };

            var filePath = Server.MapPath(file.FileName);

            MemberSession.Advanced.MaxNumberOfRequestsPerSession = 2000;

            using (var stream = file.InputStream)
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var conf = new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = a => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = true
                        }
                    };
                    var dataSet = reader.AsDataSet(conf);
                    var sheet = dataSet.Tables[0].Rows.Cast<DataRow>();
                    foreach (var row in sheet)
                    {
                        var lineNo = row.Table.Rows.IndexOf(row) + 2;

                        var prcNumber = row[0];
                        var lastName = row[1];
                        var firstName = row[2];

                        UpdateAttendance(cpdEvent, results, lineNo.ToString(), prcNumber.ToString(), firstName.ToString(), lastName.ToString());
                    }
                }
            }

            return results;
        }

        private MemberUploadResult ProcessCsv(StreamReader csvReader, CpdEvent cpdEvent)
        {
            var results = new MemberUploadResult
            {
                FailedUploads = new List<string>()
            };

            string lineNo = "";
            var firstLine = 0;

            // Hack: allows us to hit it for 2000 requests in this session
            MemberSession.Advanced.MaxNumberOfRequestsPerSession = 2000;

            while ((lineNo = csvReader.ReadLine()) != null)
            {
                // First line would be the headers which we don't want to process
                if (firstLine > 0)
                {
                    var parsedMember = ParseNameAndPrc(lineNo.Split(new Char[] { ',' }));
                    UpdateAttendance(cpdEvent, results, lineNo, parsedMember.PRC, parsedMember.FirstName, parsedMember.LastName);
                }
                firstLine++;
            }

            return results;
        }

        private void UpdateAttendance(CpdEvent cpdEvent, MemberUploadResult results, string lineNo, string prc, string firstName, string lastName)
        {
            if (!string.IsNullOrEmpty(prc))
            {
                var member = MemberSession.Query<Member>().Where(x => x.PRC.Equals(prc) && !x.Deleted).FirstOrDefault();
                if (member != null)
                {
                    AddAttendance(member, cpdEvent);
                    results.SuccessfulUploads++;
                }
                else
                {
                    results.FailedUploads.Add("PRC Specified (" + prc + ") was not found in the database: " + firstName + " " + lastName);
                }
            }
            else
            {
                var rawinfo = string.Join("", lineNo);
                results.FailedUploads.Add("No PRC Specified: " + rawinfo);
            }
        }

        private Member ParseNameAndPrc(string[] csvString)
        {
            var result = new Member();

            if (csvString != null && csvString.Any() && !string.IsNullOrEmpty(csvString.First()))
            {
                // PRC is the first column, name afterwards
                result.FirstName = csvString.ElementAtOrDefault(1);
                result.MiddleName = csvString.ElementAtOrDefault(2);
                result.MiddleName = csvString.ElementAtOrDefault(3);
                result.PRC = csvString.ElementAtOrDefault(0);
            }

            return result;
        }

        private static bool FileIsValidForUpload(HttpPostedFileBase File)
        {
            return File != null && File.ContentLength > 0 && (File.FileName.Contains(".csv") || File.FileName.Contains(".xls") || File.FileName.Contains(".xlsx"));
        }

        public ActionResult Upload(string id)
        {
            if (IsAdmin())
            {
                var viewModel = MemberSession.Load<CpdEvent>("CpdEvents/" + id);

                return View(viewModel);
            }

            return RedirectToAction("Index");
        }

        public ActionResult NonMemberLogin(string id)
        {
            if (Request.Cookies.Get("role")
                              .Value == "administrator")
            {
                var viewModel = MemberSession.Load<CpdEvent>("CpdEvents/" + id);

                return View(viewModel);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult NonMemberLogin(EventAtendee attendee, string id)
        {
            var cpdEvent = MemberSession.Load<CpdEvent>("CpdEvents/" + id);
            cpdEvent.NonMemberAttendees = cpdEvent.NonMemberAttendees ?? new List<EventAtendee>();

            cpdEvent.NonMemberAttendees.Add(attendee);

            var eventId = int.Parse(cpdEvent.Id.Split('/')[1]);
            return RedirectToAction("Confirm", "Events", new { @id = eventId });
        }

        [HttpPost]
        public ActionResult Login(string prcnumber, string id)
        {
            var cpdEvent = MemberSession.Load<CpdEvent>("CpdEvents/" + id);
            cpdEvent.MemberAttendees = cpdEvent.MemberAttendees ?? new List<string>();

            var cpdname = MemberSession.Query<CpdName>().FirstOrDefault();
            if (!cpdname.Names.Contains(cpdEvent.Name))
            {
                cpdname.Names.Add(cpdEvent.Name);
            }
            var member = MemberSession.Query<Member>().Where(x => x.PRC.Equals(prcnumber) && !x.Deleted).FirstOrDefault();

            AddAttendance(member, cpdEvent);

            var eventId = int.Parse(cpdEvent.Id.Split('/')[1]);
            return RedirectToAction("Confirm", "Events", new { @id = eventId });
        }

        private void AddAttendance(Member member, CpdEvent cpdEvent)
        {
            cpdEvent.MemberAttendees = cpdEvent.MemberAttendees ?? new List<string>();

            if (member != null && !cpdEvent.MemberAttendees.Contains(member.Id))
            {
                member.EventAttendance = member.EventAttendance ?? new List<string>();
                member.EventAttendance.Add(cpdEvent.Id);
                cpdEvent.MemberAttendees.Add(member.Id);
            }
        }

        public JsonResult LookUpPrc(string prcnumber)
        {
            try
            {
                var result = new GenericJsonResult();
                var member = MemberSession.Query<Member>().Where(x => x.PRC.Equals(prcnumber)).FirstOrDefault();

                if (member != null && !string.IsNullOrEmpty(prcnumber))
                {
                    result.Success = true;
                    result.Message = member.FirstName + " " + member.LastName;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Member not found";
                }

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new GenericJsonResult
                {
                    Message = ex.Message,
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Confirm(int id)
        {
            return View(id);
        }

        [HttpPost]
        public ActionResult Add(EventViewModel model)
        {
            MemberSession.Store(model.Event);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var cpdEvent = MemberSession.Load<CpdEvent>("CpdEvents/" + id);
            var viewModel = new EventViewModel
            {
                Event = cpdEvent
            };
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(EventViewModel model)
        {
            var oldEvent = MemberSession.Load<CpdEvent>(model.Event.Id);

            MapEvents(model.Event, oldEvent);

            MemberSession.Store(oldEvent);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            var cpdEvent = MemberSession.Load<CpdEvent>(id);
            if (id != null && cpdEvent != null)
            {
                cpdEvent.IsDeleted = true;
                MemberSession.Store(cpdEvent);
            }

            return RedirectToAction("Index");
        }

        private static void MapEvents(CpdEvent model, CpdEvent oldEvent)
        {
            oldEvent.Name = model.Name;
            oldEvent.Venue = model.Venue;
            oldEvent.Moderators = model.Moderators;
            oldEvent.Speakers = model.Speakers;
            oldEvent.Venue = model.Venue;
            oldEvent.CpdUnitType = model.CpdUnitType;
            oldEvent.CpdCredits = model.CpdUnitType == CpdUnitType.Units ? model.CpdCredits : null;
            oldEvent.Date = model.Date;
        }

        public ActionResult ExportData(string id)
        {
            var eventAttendees = new List<EventAtendee>();
            var cpdEvent = MemberSession.Load<CpdEvent>("CpdEvents/" + id);

            var ppsMembers = MemberSession.Load<Member>(cpdEvent.MemberAttendees);
            var ppsMemberConverted = ppsMembers.Select(x => new EventAtendee { FirstName = x.FirstName, LastName = x.LastName, PRC = x.PRC, PpsMember = true, MiddleName = x.MiddleName }).ToList();
            var nonMemberAttendees = cpdEvent.NonMemberAttendees;

            if (ppsMemberConverted != null)
            {
                eventAttendees.AddRange(ppsMemberConverted);
            }
            if (nonMemberAttendees != null)
            {
                eventAttendees.AddRange(nonMemberAttendees);
            }

            var stringBuilder = new StringBuilder();
            var firstRow = string.Format("Event Name,{0},Venue,{1}", cpdEvent.Name, cpdEvent.Venue);
            var secondRow = string.Format("Chapter/Society,{0},Date,{1},Time,{2}", "Southern Tagalog", cpdEvent.Date.ToShortDateString(), string.Empty);
            var thirdRow = string.Format("Number Of Participants,{0},CME Points,{1}", eventAttendees.Count, cpdEvent.CpdCredits);

            var listOfAttendees = "List of Participants";
            var headers =
                "Family Name,First Name,Middle Name, PRC Number, PPS Member?,Type of Participant,Points Earned";

            stringBuilder.AppendLine(firstRow);
            stringBuilder.AppendLine(secondRow);
            stringBuilder.AppendLine(thirdRow);
            stringBuilder.AppendLine(listOfAttendees);
            stringBuilder.AppendLine(headers);

            foreach (var attendee in eventAttendees)
            {
                var line = string.Format("{0},{1},{2},{3},{4},{5},{6}", attendee.LastName, attendee.FirstName,
                    attendee.MiddleName, attendee.PRC, attendee.PpsMember ? "Yes" : "No", "Participant", cpdEvent.CpdCredits);
                stringBuilder.AppendLine(line);
            }
            var bytes = UnicodeEncoding.Unicode.GetBytes(stringBuilder.ToString());
            return File(bytes, "text/csv", "EventAttendance.csv");
            //return result;
        }
    }
}
