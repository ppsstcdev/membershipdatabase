﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PPSSTC.Indeces;
using PPSSTC.Models;
using PPSSTC.Models.ViewModels;
using Raven.Client;
using Raven.Client.Linq;
using System.Globalization;
using System.Text;
using PPSSTC.Helpers.CSV_Models;
using System.IO;
using ExcelDataReader;
using System.Data;

namespace PPSSTC.Controllers
{
    [Authorize]
    public class MembersController : RavenController
    {
        private int _pageSize = 15;

        public ActionResult Index(MemberListViewModel model)
        {
            model.Page = 1;

            var newViewModel = BuildViewmodel(model);

            return View(newViewModel);
        }

        public ActionResult Search(MemberListViewModel model)
        {
            var newViewModel = BuildViewmodel(model);

            return View("Index", newViewModel);
        }

        private MemberListViewModel BuildViewmodel(MemberListViewModel model)
        {
            model.PageSize = _pageSize;
            var members = BuildMemberQuery(model);

            RavenQueryStatistics stats;

            members = members.OrderBy(x => x.LastName).Statistics(out stats);

            var pagedMembers = members.As<Member>().Skip((model.Page - 1) * model.PageSize).Take(model.PageSize).ToList();

            model.Members = pagedMembers;

            model.TotalCount = stats.TotalResults;

            return model;
        }

        private IRavenQueryable<MembersIndex.MemberResult> BuildMemberQuery(MemberListViewModel model)
        {
            var userRole = Request.Cookies["role"].Value;
            var members = MemberSession.Query<MembersIndex.MemberResult, MembersIndex>();

            if (!string.IsNullOrEmpty(model.Name))
            {
                members = members.Search(x => x.Name, model.Name, escapeQueryOptions: EscapeQueryOptions.RawQuery);
            }

            if (model.Status != null)
            {
                members = (IRavenQueryable<MembersIndex.MemberResult>)members.Where(x => x.Status == model.Status);
            }

            if (model.Area != null)
            {
                members = (IRavenQueryable<MembersIndex.MemberResult>)members.Where(x => x.Area == model.Area);
            }

            if (model.SubArea != null)
            {
                members = (IRavenQueryable<MembersIndex.MemberResult>)members.Where(x => x.SubArea == model.SubArea);
            }

            if (model.Subspecialty != null)
            {
                members = (IRavenQueryable<MembersIndex.MemberResult>)members.Where(x => x.Subspecialty == model.Subspecialty);
            }

            if (model.Year.HasValue)
            {
                members = members.Where(x => x.paymentYears == null || x.paymentYears.Any(y => y != model.Year.Value));
            }

            if (userRole == "administrator")
            {
                // Only show not deleted members
                members = members.Where(x => !x.Deleted);
            }
            else
            {
                // Only show not deleted and alive members
                members = members.Where(x => !x.Deleted && !x.Deceased);
            }

            return members;
        }

        public ActionResult Add()
        {
            if (Request.Cookies.Get("role")
                .Value == "administrator")
            {
                var viewModel = new EditMemberModel { Member = new Member() };
                return View(viewModel);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Add(EditMemberModel model)
        {
            MemberSession.Store(model.Member);
            return RedirectToAction("Index");
        }

        public ActionResult Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                id = HttpContext.Request.Cookies.Get("memberId").Value;
            }
            else
            {
                id = "members/" + id;
            }

            var member = MemberSession.Load<Member>(id);
            var cpdEvents = member.EventAttendance != null ? MemberSession.Load<CpdEvent>(member.EventAttendance).Where(x => x != null && !x.IsDeleted).ToList() : new List<CpdEvent>();
            var units = cpdEvents.Select(x => new CpdUnits { Date = x.Date, Name = x.Name, UnitType = x.CpdUnitType, Units = x.CpdCredits ?? (float?)null });

            var allUnits = new List<CpdUnits>();
            if (member.Units != null && member.Units.Count > 0)
            {
                allUnits.AddRange(member.Units);
            }
            allUnits.AddRange(units);

            // Join the two lists together. Member.units would have manually added credits

            var dictionary = allUnits != null ? allUnits.OrderByDescending(x => x.Date)
                                         .GroupBy(x => x.Date.Year + " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(x.Date.Month))
                                         .ToDictionary(key => key.Key, v => v.ToList())
                                         : null;


            return View(new MemberDetailViewModel
            {
                Member = member,
                CpdUnits = dictionary
            });
        }

        public ActionResult Edit(string id)
        {
            id = "members/" + id;
            var member = MemberSession.Load<Member>(id);
            var viewModel = new EditMemberModel();
            viewModel.Member = member;

            return View(viewModel);
        }

        public JsonResult AddPayment(string id, int? year)
        {
            var result = new GenericJsonResult();
            try
            {
                var member = MemberSession.Load<Member>(id);
                if (member.PaymentInfo == null)
                {
                    member.PaymentInfo = new List<PaymentInfo>();
                }
                if (member.PaymentInfo != null && member.PaymentInfo.Find(x => x.Year == year) == null)
                {
                    member.PaymentInfo.Add(new PaymentInfo { Year = year });
                }
                
                MemberSession.Store(member);

                result.Success = true;
                result.Message = "Successfully added payment information";
            }
            catch
            {
                result.Success = false;
                result.Message = "Unable to add payment information";
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            var user = MemberSession.Load<Member>(id);
            if (id != null && user != null)
            {
                user.Deleted = true;
                MemberSession.Store(user);
            }

            return RedirectToAction("Index");
        }

        public JsonResult AddCpdUnits(string id, DateTime date, string name, int units)
        {
            var result = new GenericJsonResult();
            try
            {
                var member = MemberSession.Load<Member>(id);
                if (member.Units == null)
                {
                    member.Units = new List<CpdUnits>();
                }

                var cpdUnit = new CpdUnits
                {
                    Date = date,
                    Name = name,
                    Units = units
                };

                member.Units.Add(cpdUnit);
                MemberSession.Store(member);

                result.Success = true;
                result.Message = "Successfully added CPD units";
            }
            catch
            {
                result.Success = false;
                result.Message = "Unable to add CPD units";
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult Edit(EditMemberModel model)
        {
            var oldMember = MemberSession.Load<Member>(model.Member.Id);

            MapToMember(model, oldMember);

            MemberSession.Store(oldMember);

            return RedirectToAction("Details", new { @id = oldMember.Id.Split('/')[1] });
        }

        public Helpers.CsvActionResult<PatientsCsvWrapper> ExportData(MemberListViewModel searchFilters)
        {
            var memberQuery = BuildMemberQuery(searchFilters);
            var page = 0;
            var pageSize = 100;
            var patients = new List<MemberCsvRow>();
            var keepGoing = true;
            do
            {
                var currentPatients = memberQuery.As<Member>().Skip(page * pageSize).Take(pageSize);
                if (currentPatients.FirstOrDefault() != null)
                {
                    patients.AddRange(currentPatients.Select(x =>
                    new MemberCsvRow
                    {
                        Area = x.Area,
                        Email = x.Email,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        MiddleName = x.MiddleName,
                        MobileNumber = x.MobileNumber,
                        PhilHealth = x.PhilHealth,
                        PMA = x.PMA,
                        PRC = x.PRC,
                        Status = x.Status,
                        Subspecialty = x.Subspecialty,
                        DateDeceased = x.DateDeceased,
                        DateOfBirth = x.DateOfBirth
                    }));
                    page++;
                }
                else
                {
                    keepGoing = false;
                }
            } while (keepGoing);

            var patientsCsv = patients.Select(x => new PatientsCsvWrapper
            {
                Area = x.Area,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName,
                MiddleName = x.MiddleName,
                MobileNumber = x.MobileNumber,
                PhilHealth = x.PhilHealth,
                PMA = x.PMA,
                PRC = x.PRC,
                Status = x.Status,
                Subspecialty = x.Subspecialty,
                DateDeceased = x.DateDeceased,
                DateOfBirth = x.DateOfBirth != DateTime.MinValue ? x.DateOfBirth.ToShortDateString() : ""
            }).ToList();

            var stringBuilder = new StringBuilder();

            var result = new PPSSTC.Helpers.CsvActionResult<PatientsCsvWrapper>(patientsCsv, "MemberReport.csv");
            return result;
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase File, int? year)
        {
            var results = new MemberUploadResult
            {
                FailedUploads = new List<string>()
            };

            if (File == null)
            {
                return View("UploadResults", new MemberUploadResult
                {
                    Error = "Please upload a file"
                });
            }

            if (!FileIsValidForUpload(File))
            {
                return View("UploadResults", new MemberUploadResult
                {
                    Error = "File is either empty or not a .csv/.xls/.xlsx file. Cannot Proceed with the upload"
                });
            }

            if (File.FileName.Contains(".xlsx") || File.FileName.Contains(".xls"))
            {
                results = ProcessXlsx(File, year);
            }
            else
            {
                StreamReader csvReader = new StreamReader(File.InputStream);

                results = ProcessCsv(csvReader, year);

                csvReader.Close();
            }

            return View("UploadResults", results);
        }

        private MemberUploadResult ProcessXlsx(HttpPostedFileBase file, int? year)
        {
            var results = new MemberUploadResult
            {
                FailedUploads = new List<string>()
            };

            var filePath = Server.MapPath(file.FileName);

            MemberSession.Advanced.MaxNumberOfRequestsPerSession = 2000;

            using (var stream = file.InputStream)
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var conf = new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = a => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = true
                        }
                    };
                    var dataSet = reader.AsDataSet(conf);
                    var sheet = dataSet.Tables[0].Rows.Cast<DataRow>();
                    foreach (var row in sheet)
                    {
                        var lineNo = row.Table.Rows.IndexOf(row) + 2;

                        var prcNumber = row[0];
                        var lastName = row[1];
                        var firstName = row[2];

                        UpdatePayment(year, results, lineNo.ToString(), prcNumber.ToString(), firstName.ToString(), lastName.ToString());
                    }
                }
            }

            return results;
        }

        private MemberUploadResult ProcessCsv(StreamReader csvReader, int? year)
        {
            var results = new MemberUploadResult
            {
                FailedUploads = new List<string>()
            };

            string lineNo = "";
            var firstLine = 0;

            // Hack: allows us to hit it for 2000 requests in this session
            MemberSession.Advanced.MaxNumberOfRequestsPerSession = 2000;

            while ((lineNo = csvReader.ReadLine()) != null)
            {
                // First line would be the headers which we don't want to process
                if (firstLine > 0)
                {
                    var parsedMember = ParseNameAndPrc(lineNo.Split(new Char[] { ',' }));
                    UpdatePayment(year, results, lineNo, parsedMember.PRC, parsedMember.FirstName, parsedMember.LastName);
                }
                firstLine++;
            }

            return results;
        }

        private void UpdatePayment(int? year, MemberUploadResult results, string lineNo, string prc, string firstName, string lastName)
        {
            if (!string.IsNullOrEmpty(prc))
            {
                var member = MemberSession.Query<Member>().Where(x => x.PRC.Equals(prc) && !x.Deleted).FirstOrDefault();
                if (member != null)
                {
                    AddPayment(member.Id, year);
                    results.SuccessfulUploads++;
                }
                else
                {
                    results.FailedUploads.Add("PRC Specified (" + prc + ") was not found in the database: " + firstName + " " + lastName);
                }
            }
            else
            {
                var rawinfo = string.Join("", lineNo);
                results.FailedUploads.Add("No PRC Specified: " + rawinfo);
            }
        }

        private Member ParseNameAndPrc(string[] csvString)
        {
            var result = new Member();

            if (csvString != null && csvString.Any() && !string.IsNullOrEmpty(csvString.First()))
            {
                // PRC is the first column, name afterwards
                result.FirstName = csvString.ElementAtOrDefault(1);
                result.MiddleName = csvString.ElementAtOrDefault(2);
                result.MiddleName = csvString.ElementAtOrDefault(3);
                result.PRC = csvString.ElementAtOrDefault(0);
            }

            return result;
        }

        private static bool FileIsValidForUpload(HttpPostedFileBase File)
        {
            return File != null && File.ContentLength > 0 && (File.FileName.Contains(".csv") || File.FileName.Contains(".xls") || File.FileName.Contains(".xlsx"));
        }

        //public ActionResult Upload(string id)
        //{
        //    var viewModel = MemberSession.Load<CpdEvent>("CpdEvents/" + id);

        //    return View(viewModel);
        //}

        private static void MapToMember(EditMemberModel model, Member oldMember)
        {
            oldMember.Area = model.Member.Area;
            oldMember.SubArea = model.Member.SubArea;
            oldMember.Comments = model.Member.Comments;
            oldMember.DateOfBirth = model.Member.DateOfBirth;
            oldMember.Deceased = model.Member.Deceased;
            oldMember.DateDeceased = model.Member.Deceased ? model.Member.DateDeceased : null;
            oldMember.Email = model.Member.Email;
            oldMember.FirstName = model.Member.FirstName;
            oldMember.LastName = model.Member.LastName;
            oldMember.MiddleName = model.Member.MiddleName;
            oldMember.PMA = model.Member.PMA;
            oldMember.PRC = model.Member.PRC;
            oldMember.PhilHealth = model.Member.PhilHealth;
            oldMember.Status = model.Member.Status;
            oldMember.Subspecialty = model.Member.Subspecialty;
            oldMember.MobileNumber = model.Member.MobileNumber;
        }
    }
}
