﻿using PPSSTC.Models;
using PPSSTC.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PPSSTC.Controllers
{
    public class ProfileController : RavenController
    {
        //
        // GET: /Profile/
        public ActionResult Index(string id)
        {
            id = "members/" + id;
            var member = MemberSession.Load<Member>(id);
            return View(member);
        }

        public ActionResult Edit(string id)
        {
            id = "members/" + id;
            var member = MemberSession.Load<Member>(id);
            var viewModel = new EditMemberModel();
            viewModel.Member = member;

            return View(viewModel);
        }
    }
}
