﻿using Excel;
using PPSSTC.Models;
using Raven.Client.Document;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPSSTC.DataImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            //ImportFromExcel();
            DocumentStore MemberStore = new DocumentStore { ConnectionStringName = "MemberStore" };
            MemberStore.Initialize();
            using (var session = MemberStore.OpenSession())
            {
                session.Advanced.MaxNumberOfRequestsPerSession = 50000;
                var cpdEvents = session.Query<CpdEvent>().ToList();
                foreach (var cpdEvent in cpdEvents)
                {
                    if (cpdEvent.MemberAttendees != null)
                    {
                        var members = session.Load<Member>(cpdEvent.MemberAttendees);
                        foreach (var member in members)
                        {
                            var unitToRemove = member.Units.Where(x => x.Name == cpdEvent.Name && x.Units == cpdEvent.CpdCredits && x.Date == cpdEvent.Date);
                            if (unitToRemove.Any())
                            {
                                member.Units.Remove(unitToRemove.First());
                                if (member.EventAttendance == null)
                                {
                                    member.EventAttendance = new List<string>();
                                }
                                member.EventAttendance.Add(cpdEvent.Id);
                                session.Store(member);
                                session.SaveChanges();
                            }
                        } 
                    }
                }
                
            }
        }

        private static void ImportFromExcel()
        {
            //NAME	PRC	PMA

            FileStream stream = File.Open(@"D:\JMRX\pps stc members\Laguna.xls", FileMode.Open, FileAccess.Read);

            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
            IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            //...
            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
            //IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            //...
            //3. DataSet - The result of each spreadsheet will be created in the result.Tables
            DataSet result = excelReader.AsDataSet();

            var table = result.Tables[0];
            var members = new List<Member>();
            foreach (System.Data.DataRow row in table.Rows)
            {
                var member = new Member();
                member.Area = Area.Laguna_MLP;
                var name = row.ItemArray[0].ToString();
                if (!string.IsNullOrEmpty(name))
                {
                    member.FirstName = name.Split(',')[1];
                    member.LastName = name.Split(',')[0];
                    member.PRC = row.ItemArray[1].ToString();
                    member.PMA = row.ItemArray[2].ToString();
                    member.Subspecialty = Subspecialty.General_Pediatrics;
                    members.Add(member);
                }

            }

            DocumentStore MemberStore = new DocumentStore { ConnectionStringName = "MemberStore" };
            MemberStore.Initialize();
            using (var session = MemberStore.OpenSession())
            {
                foreach (var member in members)
                {
                    session.Store(member);
                }
                session.SaveChanges();
            }

            //6. Free resources (IExcelDataReader is IDisposable)
            excelReader.Close();
        }
    }
}
